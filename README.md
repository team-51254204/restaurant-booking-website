# Restaurant Booking Website

## Overview
This project consists of two main components: **Backend** and **Frontend**. Follow the instructions below to set up each part of the project.


### Backend Setup

1. Navigate to the `Backend` folder using the terminal or command prompt.

2. Install dependencies by running the following command:

   ```
   npm install

3. Changes in Backend/config/config.json

    ```
    {
        "development": {
            "username": "<YOUR_USERNAME>",
            "password": "<YOUR_PASSWORD>",
            "database": "<YOUR_DATABASE_NAME>",
            "host": "<YOUR_HOST_NAME>",
            "dialect": "<YOUR_DATABASE_SYSTEM_NAME>"
        },
        "test": {
            "username": "<YOUR_USERNAME>",
            "password": "<YOUR_PASSWORD>",
            "database": "<YOUR_DATABASE_NAME>",
            "host": "<YOUR_HOST_NAME>",
            "dialect": "<YOUR_DATABASE_SYSTEM_NAME>"
        },
        "production": {
            "username": "root",
            "password": null,
            "database": "database_production",
            "host": "127.0.0.1",
            "dialect": "mysql"
        }
    }

    Note: Make sure there should be an existing database of that name present.

4. Add a .env file:

    ```
    SECRET_KEY= "<YOUR_SECRET_KEY>"
    ACCESS_TOKEN_SECERT="<YOUR_ACCESS_TOKEN>"

5. Run Backend:

    ```
    npm run dev

### Frontend Setup

1. Navigate to the `Frontend` folder using the terminal or command prompt.

2. Install dependencies by running the following command:

   ```
   npm install

3. Run command:
    ```
    ($env:HTTPS = "true") -and (npm start)
