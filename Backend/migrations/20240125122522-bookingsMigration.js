'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Bookings', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      slot_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Slots',
          key: 'id',
        },
      },
      customer_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Customers',
          key: 'id',
        },
      },
      customer_name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      contact_number: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      booking_date: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      num_guests: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
        field: 'created_at',
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
        field: 'updated_at',
      },
    });

    // Add foreign key constraints
    await queryInterface.addConstraint('Bookings', {
      type: 'foreign key',
      fields: ['slot_id'],
      references: {
        table: 'Slots',
        field: 'id',
      },
      onDelete: 'cascade',
    });

    await queryInterface.addConstraint('Bookings', {
      type: 'foreign key',
      fields: ['customer_id'],
      references: {
        table: 'Customers',
        field: 'id',
      },
      onDelete: 'cascade',
    });
  },

  down: async (queryInterface, Sequelize) => {
    // Remove foreign key constraints
    await queryInterface.removeConstraint('Bookings', 'Bookings_slot_id_fkey');
    await queryInterface.removeConstraint('Bookings', 'Bookings_customer_id_fkey');
    await queryInterface.dropTable('Bookings');
  },
};
