'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Inventories', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      restaurant_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Restaurants',
          key: 'id',
        },
      },
      slot_id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Slots',
          key: 'id',
        },
      },
      quantity: {
        type: Sequelize.INTEGER, 
        allowNull: false,
      },
      date: {
        type: Sequelize.DATEONLY,
        allowNull: true,
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false,
      },
    });
    
    // Add index
    await queryInterface.addIndex('Inventories', ['restaurant_id', 'slot_id', 'date'], { name: 'index_restaurant_slot_date' });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex('Inventories', 'index_restaurant_slot_date');
    await queryInterface.dropTable('Inventories');
  }
};
