const { DataTypes, Sequelize } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  const Slot = sequelize.define('Slot', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    restaurant_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Restaurants',
        key: 'id',
      },
    },
    start_time: {
      type: DataTypes.TIME, 
      allowNull: false,
    },
    end_time: {
      type: DataTypes.TIME, 
      allowNull: false,
    },
    capacity: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },

    createdAt: {
      type: DataTypes.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      allowNull: false,
      field: 'created_at',
    },
    updatedAt: {
      type: DataTypes.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      allowNull: false,
      field: 'updated_at',
    },
  }, {
    timestamps: true,
    indexes: [
      {
        name: 'restaurant_start_time_index',
        unique: false,
        fields: ['restaurant_id', 'start_time'],
      },
    ],
  });

  return Slot;
};
