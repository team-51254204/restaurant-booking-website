const express = require("express");
const cors = require("cors");
const db = require("./models");
const cookieParser = require("cookie-parser");
const app = express();
const {CORS_OPTIONS} = require('./constants/apiConstant')

// Defining the port number for the server to listen on
const port = 3001;
const dotenv = require("dotenv");

// To parse incoming JSON requests
app.use(express.json());
app.use(cors(CORS_OPTIONS))
app.use(cookieParser());

dotenv.config();

app.use("/", require("./routes/customersRoute"));
app.use("/", require("./routes/restaurantsRoute"));
app.use("/", require("./routes/bookingsRoute"));
app.use("/", require ("./routes/slotRoutes"));
app.use("/", require("./routes/inventoriesRoute"));

db.sequelize.sync().then(() => {
    app.listen(port, () => {
        console.log(`Server running at http://localhost:${port}`);
    });
});

module.exports = app;