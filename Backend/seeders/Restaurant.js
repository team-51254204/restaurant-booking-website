'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Restaurants', [
      {
        name: 'Brew Waves',
        image: 'https://assets.architecturaldigest.in/photos/64f85037ec0bc118bdd98aba/16:9/w_1920,c_limit/Untitled%20design%20(14).png',
        location: 'Gurgaon',
        cuisine_type: 'Indian',
        created_at: new Date('2024-01-31 07:38:41'),
        updated_at: new Date('2024-01-31 07:38:41'),
      },
      {
        name: 'The Beer Cafe',
        image: 'https://images.venuepool.com/get_images.php?width=730&height=430&path=venue_images/1558/the-beer-cafe1.jpg',
        location: 'Gurgaon',
        cuisine_type: 'Continental',
        created_at: new Date('2024-01-31 07:39:13'),
        updated_at: new Date('2024-01-31 07:39:13'),
      },
      {
        name: 'Brew and Blend',
        image: 'https://www.shoutlo.com/assets/images/merchant_images/merchant-130005-6065767dd637d.jpg',
        location: 'Punjab',
        cuisine_type: 'Chinese',
        created_at: new Date('2024-01-31 07:39:27'),
        updated_at: new Date('2024-01-31 07:39:27'),
      },
      {
        name: 'Restro Florida Cafe',
        image: 'https://files.yappe.in/place/full/restro-cafe-florida-8212756.webp',
        location: 'Punjab',
        cuisine_type: 'French',
        created_at: new Date('2024-01-31 07:39:43'),
        updated_at: new Date('2024-01-31 07:39:43'),
      },
      {
        name: 'Barbeque Nation',
        image: 'https://etimg.etb2bimg.com/photo/93275633.cms',
        location: 'Lucknow',
        cuisine_type: 'Multi Cuisine',
        created_at: new Date('2024-01-31 07:39:55'),
        updated_at: new Date('2024-01-31 07:39:55'),
      },
      {
        name: 'The Drowning Street',
        image: 'https://media.assettype.com/knocksense%2F2022-11%2Fda76acfd-5342-4dd5-86c1-66d30a6142a0%2Fds.jpg?auto=format%2Ccompress&fit=max&format=webp&w=1536&dpr=1.3',
        location: 'Agra',
        cuisine_type: 'Multi Cuisine',
        created_at: new Date('2024-01-31 07:40:07'),
        updated_at: new Date('2024-01-31 07:40:07'),
      },
      {
        name: 'Signature Café & Restaurant',
        image: 'https://s.inyourpocket.com/gallery/176120.jpg',
        location: 'Lucknow',
        cuisine_type: 'Multi Cuisine',
        created_at: new Date('2024-01-31 07:40:20'),
        updated_at: new Date('2024-01-31 07:40:20'),
      },
      {
        name: 'The Chocolate Room',
        image: 'https://wallpapercave.com/dwp1x/Of9zOYl.jpg',
        location: 'Mohali',
        cuisine_type: 'Bakery',
        created_at: new Date('2024-01-31 07:40:37'),
        updated_at: new Date('2024-01-31 07:40:37'),
      },
      {
        name: 'Biryani by Kilo',
        image: 'https://images.indulgexpress.com/uploads/user/imagelibrary/2021/7/13/original/BiryanibyKilosHyderabadiVegBiryani.jpg',
        location: 'Mohali',
        cuisine_type: 'Indian',
        created_at: new Date('2024-01-31 07:40:47'),
        updated_at: new Date('2024-01-31 07:40:47'),
      },
      {
        name: 'Pinxx',
        image: 'https://www.royalorchidhotels.com/images/dining/28_26_2020_10_26_34Pinxx-5.jpg',
        location: 'Gurgaon',
        cuisine_type: 'Indian',
        created_at: new Date('2024-01-31 07:40:57'),
        updated_at: new Date('2024-01-31 07:40:57'),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    // Add logic to undo the changes made in the up function
    await queryInterface.bulkDelete('Restaurants', null, {});
  },
};