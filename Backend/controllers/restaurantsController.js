const asyncHandler = require('express-async-handler');
const { Restaurants, Inventory, Slot } = require('../models');

const restaurantsController = {
  selectAll: asyncHandler(async (req, res) => {
    try {
      const { queryParam } = req.query;

      let results;

      if (queryParam) {
        results = await Restaurants.findAndCountAll({
          where: { cuisine_type: queryParam },
        });

        if (results.count === 0) {
          results = await Restaurants.findAndCountAll({
            where: { location: queryParam },
          });
        }
      } else {
        results = await Restaurants.findAndCountAll();
      }

      if (results.count === 0) {
        return res.status(404).json({ error: 'No matching results found' });
      }

      res.json({
        total: results.count,
        restaurants: results.rows,
      });
    } catch (err) {
      console.error(err);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  }),

  insert: asyncHandler(async (req, res) => {
    try {
      const { name, image, location, cuisine_type } = req.body;

      if (!name || !image || !location || !cuisine_type) {
        return res.status(400).json({ error: 'Missing required fields' });
      }

      const newRestaurant = await Restaurants.create({
        name,
        image,
        location,
        cuisine_type,
      });

      res.status(201).json({ message: 'Insert successful', restaurant: newRestaurant });
    } catch (err) {
      console.error(err);
      res.status(500).json({ error: 'Error during insertion' });
    }
  }),

  delete: asyncHandler(async (req, res) => {
    try {
      const restaurantId = req.params.restaurantId;

      if (!/^\d+$/.test(restaurantId) || restaurantId <= 0) {
        return res.status(400).json({ error: 'Invalid restaurant ID' });
      }

      const existingRestaurant = await Restaurants.findByPk(restaurantId);

      if (!existingRestaurant) {
        return res.status(404).json({ error: 'Restaurant not found' });
      }

      await Restaurants.destroy({ where: { id: restaurantId } });

      res.status(200).json({ message: 'Delete successful' });
    } catch (err) {
      console.error(err);
      res.status(500).json({ error: 'Error during deletion' });
    }
  }),

  getDetailById: asyncHandler(async (req, res) => {
    try {
      const restaurantId = req.params.restaurantId;

      if (!/^\d+$/.test(restaurantId) || restaurantId <= 0) {
        return res.status(400).json({ error: 'Invalid restaurant ID' });
      }

      const Restaurant = await Restaurants.findByPk(restaurantId);

      if (!Restaurant) {
        return res.status(404).json({ error: 'Restaurant not found' });
      }

      const Inven = await Inventory.findAll({
        where: { restaurant_id: restaurantId },
        attributes: ['id', 'quantity'],
      });

      const slotDetails = await Slot.findAll({
        where: { restaurant_id: restaurantId },
        attributes: ['id', 'start_time', 'end_time', 'capacity'],
      });

      const restaurantWithSlots = {
        id: Restaurant.id,
        name: Restaurant.name,
        image: Restaurant.image,
        location: Restaurant.location,
        cuisine_type: Restaurant.cuisine_type,
        slots: slotDetails,
        quantity: Inven,
      };

      res.json(restaurantWithSlots);

    } catch (err) {
      console.error(err);
      res.status(500).json({ error: 'Error retrieving restaurant details' });
    }
  })
};

module.exports = restaurantsController;
