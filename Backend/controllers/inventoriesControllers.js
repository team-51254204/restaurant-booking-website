const { Inventory, Slot } = require('../models');

const createInventories = async (restaurantId) => {
  try {
    const restaurantSlots = await Slot.findAll({
      where: { restaurant_id: restaurantId },
    });
    const inventories = [];
    const currentDate = new Date().toISOString().split('T')[0];

     for (const slot of restaurantSlots) {
      for (let i = 0; i < 5; i++) {
        const date = new Date(currentDate);
        date.setDate(date.getDate() + i);

        inventories.push({
          restaurant_id: restaurantId,
          slot_id: slot.id,
          quantity: slot.capacity,
          date: date.toISOString().split('T')[0], 
        });
      }
    }
    await Inventory.bulkCreate(inventories);

    console.log(`Inventories created successfully for restaurant ${restaurantId}.`);
  } catch (error) {
    console.elog(error);
    throw new Error('Error creating inventories');
  }
};

const getInventoryByDetails = async (req, res) => {
  const { restaurant_id, slot_id, date } = req.query;

  try {
    const inventory = await Inventory.findOne({
      where: {
        restaurant_id,
        slot_id,
        date,
      },
    });

    if (!inventory) {
      return res.status(404).json({ error: 'Inventory not found for the given details' });
    }

    res.json(inventory);
  } catch (error) {
    console.log(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

module.exports = { createInventories, getInventoryByDetails };
