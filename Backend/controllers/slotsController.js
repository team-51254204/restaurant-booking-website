const { Slot, Restaurants } = require('../models');

const slots = async (restaurantId) => {
  try {
    const restaurant = await Restaurants.findByPk(restaurantId);

    if (!restaurant) {
      throw new Error('Restaurant not found');
    }

    let startTime = new Date().toLocaleString("en-US", {timeZone: "Asia/Kolkata"});
    startTime = new Date(startTime);
    startTime.setHours(9, 0, 0, 0);
    let endTime = new Date(startTime);
    endTime.setHours(24, 0, 0, 0);
    const slotDuration = 30 * 60 * 1000;
    const capacity = 50;

    const slots = [];

    for (let time = startTime; time < endTime; time = new Date(time.getTime() + slotDuration)) {
      let endSlotTime = new Date(time.getTime() + slotDuration);
      let formattedEndTime = endSlotTime.toLocaleTimeString('en-US', { timeZone: 'Asia/Kolkata', hour12: false, hour: 'numeric', minute: '2-digit' });
      if (formattedEndTime === "24:00") {
        formattedEndTime = "00:00";
      }
      const slot = {
        restaurant_id: restaurantId,
        restaurant_name: restaurant.name,
        start_time: time.toLocaleTimeString('en-US', { timeZone: 'Asia/Kolkata', hour12: false, hour: 'numeric', minute: '2-digit' }).slice(0, 5),
        end_time: formattedEndTime.slice(0, 5),
        capacity: capacity,
      };
      slots.push(slot);
    }

    await Slot.bulkCreate(slots);

    console.log('Slots created successfully.');
  } catch (error) {
    console.log(error);
    throw new Error('Error creating slots');
  }
};

const getSlotByTime = async (req, res) => {
  const { restaurantId, time } = req.params;

  try {

    const formattedTime = `${time}:00`.slice(0, 5);

    const slot = await Slot.findOne({
      where: {
        restaurant_id: restaurantId,
        start_time: formattedTime,
      },
    });

    if (!slot) {
      return res.status(404).json({ error: 'Slot not found' });
    }

    return res.status(200).json(slot);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: 'Internal server error' });
  }
};

module.exports = { slots, getSlotByTime };