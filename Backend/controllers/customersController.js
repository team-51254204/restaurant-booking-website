const { Customer } = require('../models');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const asyncHandler = require("express-async-handler");
const { isEmail } = require('validator');
const { COOKIE_OPTIONS } = require("../constants/apiConstant")

const registerUser = asyncHandler(async (req, res) => {
  const { name, email, password } = req.body;

  try {
    if (!name || !/^[a-zA-Z\s]*$/.test(name)) {
      return res.status(400).json({ message: 'Please enter a valid name' });
    }
    if (!email) {
      return res.status(400).json({ message: 'Please enter email' });
    }
    if (!password) {
      return res.status(400).json({ message: 'Please enter password' });
    }

    if (!isEmail(email)) {
      return res.status(400).json({ message: 'Invalid email format' });
    }

    const existingUser = await Customer.findOne({ where: { email } });

    if (existingUser) {
      return res.status(400).json({ message: 'Email is already in use' });
    }

    const hashedPassword = await bcrypt.hash(password, 10);

    const newUser = await Customer.create({
      name,
      email,
      password: hashedPassword,
    });

    const payload = {
      userId: newUser.id,
      customer_id: newUser.id,
      customer_name: newUser.name,
      customer_email: newUser.email,
    };

    const token = jwt.sign(payload, process.env.SECRET_KEY, { expiresIn: '1h' });
    res.status(201)
      .cookie("AccessToken", token, COOKIE_OPTIONS)
      .json({ message: 'User registered successfully', token });
  } catch (error) {
    console.error('Error registering user:', error);
    if (error.name === 'SequelizeValidationError') {
      const validationErrors = error.errors.map((e) => e.message);
      res.status(400).json({ message: 'Validation error', errors: validationErrors });
    } else {
      res.status(500).json({ message: 'Internal server error' });
    }
  }
});

const loginUser = asyncHandler(async (req, res) => {
  const { email, password } = req.body;

  try {
    if (!email) {
      return res.status(400).json({ message: 'Please enter email' });
    }
    if (!password) {
      return res.status(400).json({ message: 'Please enter password' });
    }
    if (!isEmail(email)) {
      return res.status(400).json({ message: 'Invalid email format' });
    }

    const user = await Customer.findOne({ where: { email } });
    if (user && await bcrypt.compare(password, user.password)) {
      const payload = {
        userId: user.id,
        customer_id: user.id,
        customer_name: user.name,
        customer_email: user.email,
      };

      const token = jwt.sign(payload, process.env.SECRET_KEY, { expiresIn: '1h' });

      return res 
        .status(200)
        .cookie("AccessToken", token, COOKIE_OPTIONS)
        .json({ message: 'Login successful' });
    } else {
      res.status(401).json({ message: 'Invalid email or password' });
    }
  } catch (error) {
    console.error('Error logging in:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
});

module.exports = {
  registerUser,
  loginUser,
};
