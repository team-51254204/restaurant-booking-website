const asyncHandler = require('express-async-handler');
const { Restaurants, Booking, Slot, Inventory, sequelize } = require('../models');
const { Transaction } = require('sequelize');

const isValidPhoneNumber = (phoneNumber) => {
  return /^[0-9]{10}$/.test(phoneNumber);
};

const bookingController = {
  createReservation: asyncHandler(async (req, res) => {
    const t = await sequelize.transaction({ isolationLevel: Transaction.ISOLATION_LEVELS.SERIALIZABLE
    });

    try {
      const { slot_id, customer_name, contact_number, booking_date, num_guests } = req.body;
      const customer_id = req.userId;
      const slotDetail = await Slot.findByPk(slot_id, { transaction: t });
      const inventoryDetail = await Inventory.findOne({ where: { slot_id, date: booking_date }, transaction: t });

      if (!inventoryDetail) {
        await t.rollback();
        return res.status(400).json({ error: 'Inventory not found for the specified slot' });
      }

      const restaurant_id = slotDetail.restaurant_id;
      const restaurantDetail = await Restaurants.findByPk(restaurant_id, { transaction: t });

      if (num_guests <= 0 || !/^[a-zA-Z]+$/.test(customer_name) || !isValidPhoneNumber(contact_number)) {
        await t.rollback();
        return res.status(400).json({ error: 'Invalid input. Please check your reservation details.' });
      }

      await Booking.create({
        slot_id,
        customer_id,
        customer_name,
        contact_number,
        booking_date,
        num_guests,
      }, { transaction: t });

      inventoryDetail.quantity -= num_guests;

      await inventoryDetail.save({ transaction: t });

      if (inventoryDetail.quantity < 0) {
        await t.rollback();
        return res.status(400).json({ error: 'Restaurant is full. Please choose another slot.' });
      }

      await t.commit();

      const bookingDetails = {
        restaurant_name: restaurantDetail.name,
        restaurant_location: restaurantDetail.location,
        cuisine_type: restaurantDetail.cuisine_type,
        customer_name,
        num_guests,
        booking_date,
      };

      res.status(201).json({ message: 'Reservation created successfully', reservation: bookingDetails });
    } catch (err) {
      await t.rollback();
      console.log(err);
      res.status(500).json({ error: 'Error during reservation creation', details: err.message });
    }
  }),
};

module.exports = bookingController;
