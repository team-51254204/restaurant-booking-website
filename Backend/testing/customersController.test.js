const request = require('supertest');
const app = require('../index');
const { Customer } = require('../models');
const bcrypt = require('bcrypt');
describe('Login-Signup Testing', () => {
  let user;

  beforeEach(async () => {
    // Insert a sample restaurant before each test
    user = await Customer.create({
      name: 'Test User',
      email: 'test@example.com',
      password: await bcrypt.hash('password', 10),
    });
  });

  afterEach(async () => {
    // Delete the sample restaurant after each test
    await Customer.destroy({ where: { id: user.id } });
  });

  describe('POST /v1/auth/signup', () => {
    it('should register a new user', async () => {
      const response = await request(app)
        .post('/v1/auth/signup')
        .send({
          name: 'test',
          email: 'test@example.com',
          password: 'newpassword',
        });

      expect(response.statusCode).toBe(201);
      expect(response.body).toHaveProperty('message', 'User registered successfully');
      expect(response.body).toHaveProperty('user');
    });

    it('should return 400 if email is already in use', async () => {
      const response = await request(app)
        .post('/v1/auth/signup')
        .send({
          name: 'Duplicate User',
          email: 'test@example.com',
          password: 'newpassword',
        });

      expect(response.statusCode).toBe(400);
      expect(response.body).toHaveProperty('message', 'Email is already in use');
    });
  });

  describe('POST /v1/auth/signin', () => {
    it('should log in a user and return a valid access token', async () => {
      const response = await request(app)
        .post('/v1/auth/signin')
        .send({
          email: 'test@example.com',
          password: 'password',
        });

      expect(response.statusCode).toBe(200);

      expect(response.body).toHaveProperty('message', 'Login successful');
      expect(response.body).toHaveProperty('response');
      expect(response.body.response).toHaveProperty('content');
      expect(response.body.response.content).toHaveProperty('meta');
      expect(response.body.response.content.meta).toHaveProperty('access_token');
      
      const cookies = response.headers['set-cookie'];
      expect(cookies.some(cookie => cookie.includes('AccessToken'))).toBe(true);
    });

    it('should return 401 for invalid email or password during login', async () => {
      const response = await request(app)
        .post('/v1/auth/signin')
        .send({
          email: 'invalid@example.com',
          password: 'invalidpassword',
        });

      expect(response.statusCode).toBe(401);
      expect(response.body).toHaveProperty('message', 'Invalid email or password');
    });
  });
});
