const { createInventories, getInventoryById } = require('../controllers/inventoriesControllers');
const { Inventory } = require('../models');
jest.mock('../models');

//Test case #1:test function to create or insert inventory
describe('Inventory Controller', () => {
  describe('createInventories', () => {
    it('should create inventories for a specific restaurant', async () => {
      const restaurantId = 1;
      const mockSlots = [{ id: 1 }, { id: 2 }]; 
      Inventory.bulkCreate.mockResolvedValue(); 
      const Slot = require('../models').Slot;
      Slot.findAll.mockResolvedValue(mockSlots);

      await createInventories(restaurantId);
      expect(Slot.findAll).toHaveBeenCalledWith({ where: { restaurant_id: restaurantId } });
      expect(Inventory.bulkCreate).toHaveBeenCalled();
    });

    //Test case #2:test function to check if it is giving error in inventory creation

    it('should throw an error if inventories creation fails', async () => {
      const restaurantId = 1;
      Inventory.bulkCreate.mockRejectedValue(new Error('Database error'));
      await expect(createInventories(restaurantId)).rejects.toThrow('Error creating inventories');
      expect(Inventory.bulkCreate).toHaveBeenCalled();
    });
  });

  // Test case #3:test function to check if it is fetching inventory id
  describe('getInventoryById', () => {
    it('should return the inventory with the specified ID', async () => {
      const inventoryId = 1;
      const mockInventory = { id: inventoryId, quantity: 50 }; 
      Inventory.findByPk.mockResolvedValue(mockInventory); 
      const req = { params: { id: inventoryId } };
      const res = {
        json: jest.fn(),
        status: jest.fn().mockReturnThis(),
      };
      await getInventoryById(req, res);
      expect(Inventory.findByPk).toHaveBeenCalledWith(inventoryId);
      expect(res.json).toHaveBeenCalledWith(mockInventory);
    });

    // Test case #4:test function to return error if inventory not found
    it('should return 404 if inventory is not found', async () => {
      const inventoryId = 1;
      Inventory.findByPk.mockResolvedValue(null); 
      const req = { params: { id: inventoryId } };
      const res = {
        json: jest.fn(),
        status: jest.fn().mockReturnThis(),
      };
      await getInventoryById(req, res);
      expect(Inventory.findByPk).toHaveBeenCalledWith(inventoryId);
      expect(res.status).toHaveBeenCalledWith(404);
      expect(res.json).toHaveBeenCalledWith({ error: 'Inventory not found' });
    });

    //Test case #5:test function for testing internal serval error
    it('should handle internal server error', async () => {
      const inventoryId = 1;
      Inventory.findByPk.mockRejectedValue(new Error('Database error')); 
      const req = { params: { id: inventoryId } };
      const res = {
        json: jest.fn(),
        status: jest.fn().mockReturnThis(),
      };
      await getInventoryById(req, res);
      expect(Inventory.findByPk).toHaveBeenCalledWith(inventoryId);
      expect(res.status).toHaveBeenCalledWith(500);
      expect(res.json).toHaveBeenCalledWith({ error: 'Internal Server Error' });
    });
  });
  afterAll(() => {
    updateQuantityCronJob.stop();
  });
});
