const request = require('supertest');
const express = require('express');
const bodyParser = require('body-parser');
const bookingController = require('../controllers/bookingsController');
const app = express();
app.use(bodyParser.json());
const { Inventory } = require('../models');

//Test case #1:test function to check if inventory is available or not
bookingController.createReservation = async (req, res) => {
  const { slot_id, customer_name, contact_number, booking_date, num_guests } = req.body;
  const inventoryDetail = await Inventory.findOne({ where: { slot_id } });
  if (!inventoryDetail) {
    return res.status(400).json({ error: 'Inventory not found for the specified slot' });
  }
  //sample test data
  const reservation = {
    restaurant_name: 'Test Restaurant',
    restaurant_location: 'Test Location',
    cuisine_type: 'Test Cuisine',
    customer_name,
    contact_number,
    booking_date,
    num_guests,
  };
  return res.status(201).json({ message: 'Reservation created successfully', reservation });
};
app.post('/reservation', bookingController.createReservation);

//Test case #2:test function to check if it is creating reservation
describe('Booking Controller', () => {
  it('should create a reservation successfully', async () => {
    const slot_id = '1';
    const customer_name = 'Priti';
    const contact_number = '1234567890';
    const booking_date = '2024-02-08';
    const num_guests = 2;
    const userId = '3'; 
    const res = await request(app)
      .post('/reservation')
      .set('Authorization', `Bearer ${userId}`) 
      .send({
        slot_id,
        customer_name,
        contact_number,
        booking_date,
        num_guests,
      });
    expect(res.statusCode).toEqual(201);
    expect(res.body.message).toEqual('Reservation created successfully');
    expect(res.body.reservation).toHaveProperty('restaurant_name');
    expect(res.body.reservation).toHaveProperty('restaurant_location');
    expect(res.body.reservation).toHaveProperty('cuisine_type');
    expect(res.body.reservation.customer_name).toEqual(customer_name);
    expect(res.body.reservation.num_guests).toEqual(num_guests);
    expect(res.body.reservation.booking_date).toEqual(booking_date);
  });

  //Test case #3:test function to chcek it with mocked inventory
  it('should return an error if inventory is not found', async () => {
    Inventory.findOne = jest.fn().mockResolvedValue(null);

    const res = await request(app)
      .post('/reservation')
      .send({
        slot_id: 'invalid-slot-id',
        customer_name: 'John Doe',
        contact_number: '1234567890',
        booking_date: '2024-02-07',
        num_guests: 2,
      });

    expect(res.statusCode).toEqual(400);
    expect(res.body.error).toEqual('Inventory not found for the specified slot');
  });
});
