const request = require('supertest');
const app = require('../index');
const { Restaurants } = require('../models');

describe('Restaurants Controller Tests', () => {
  let insertedRestaurant;

  beforeEach(async () => {
    // Insert a sample restaurant before each test
    insertedRestaurant = await Restaurants.create({
      name: 'Sample Restaurant',
      image: 'sample-restaurant.jpg',
      location: 'Sample Location',
      cuisine_type: 'sample-cuisine',
    });
  });

  afterEach(async () => {
    // Delete the sample restaurant after each test
    if (insertedRestaurant) {
      await insertedRestaurant.destroy();
    }
  });

  describe('POST /insert', () => {
    it('should insert a new restaurant', async () => {
      const response = await request(app)
        .post('/insert')
        .send({
          name: 'New Restaurant',
          image: 'new-restaurant.jpg',
          location: 'New Location',
          cuisine_type: 'new-cuisine',
        });

      expect(response.statusCode).toBe(201);
      expect(response.body.message).toBe('Insert successful');
      expect(response.body.restaurant).toBeDefined();
    });
  });

  describe('DELETE /delete/:restaurantId', () => {
    it('should delete a restaurant by id', async () => {
      const response = await request(app)
        .delete(`/delete/${insertedRestaurant.id}`);

      if (response.statusCode === 200) {
        expect(response.body.message).toBe('Delete successful');
      } else {
        expect(response.statusCode).toBe(404);
      }
    });
  });

  describe('GET /getDetailById/:restaurantId', () => {
    it('should return restaurant details with slots by ID after insertion', async () => {
      const response = await request(app)
        .get(`/getDetailById/${insertedRestaurant.id}`);

      expect(response.statusCode).toBe(200);
    });
  });
});
