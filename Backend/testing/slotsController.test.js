const { slots, getSlotByTime } = require('../controllers/slotsController');
const { Restaurants, Slot } = require('../models');
jest.mock('../models');

describe('Slot Controller', () => {
  describe('slots', () => {
    //Test case #1: testing for creating slots of restaurant

    it('should create slots for a specific restaurant', async () => {
      const restaurantId = 4;
      const mockRestaurant = { id: restaurantId, name: 'Test Restaurant' }; 
      Restaurants.findByPk.mockResolvedValue(mockRestaurant); 
      Slot.bulkCreate.mockResolvedValue();
      await slots(restaurantId);
      expect(Restaurants.findByPk).toHaveBeenCalledWith(restaurantId);
      expect(Slot.bulkCreate).toHaveBeenCalled();
    });

    //Test case #2: To check if restaurant is available
    it('should throw an error if restaurant is not found', async () => {
      const restaurantId = 9;
      Restaurants.findByPk.mockResolvedValue(null);
          await expect(slots(restaurantId)).rejects.toThrow('Error creating slots');
      expect(Restaurants.findByPk).toHaveBeenCalledWith(restaurantId);
    });
    
});

  describe('getSlotByTime', () => {
    const restaurantId = 1;
    const time = '09:00'; //start_time

    //Test case #3:test function to fetch slot at specific timings

    it('should return the slot with the specified start time', async () => {
      const mockSlot = { id: 1, start_time: time }; 
      Slot.findOne.mockResolvedValue(mockSlot); 
      const req = { params: { restaurantId, time } };
      const res = { json: jest.fn(), status: jest.fn().mockReturnThis() };
      await getSlotByTime(req, res);
      expect(Slot.findOne).toHaveBeenCalledWith({ where: { restaurant_id: restaurantId, start_time: time } });
      expect(res.json).toHaveBeenCalledWith(mockSlot);
    });

    //Test case #4:test function to check slot availability
    it('should return 404 if slot is not found', async () => {
      Slot.findOne.mockResolvedValue(null); 
      const req = { params: { restaurantId, time } };
      const res = { json: jest.fn(), status: jest.fn().mockReturnThis() };
      await getSlotByTime(req, res);
      expect(Slot.findOne).toHaveBeenCalledWith({ where: { restaurant_id: restaurantId, start_time: time } });
      expect(res.status).toHaveBeenCalledWith(404);
      expect(res.json).toHaveBeenCalledWith({ error: 'Slot not found' });
    });

    //Test case #5:test function for internal server error

    it('should handle internal server error', async () => {
      Slot.findOne.mockRejectedValue(new Error('Database error')); 
      const req = { params: { restaurantId, time } };
      const res = { json: jest.fn(), status: jest.fn().mockReturnThis() };
      await getSlotByTime(req, res);
      expect(Slot.findOne).toHaveBeenCalledWith({ where: { restaurant_id: restaurantId, start_time: time } });
      expect(res.status).toHaveBeenCalledWith(500);
      expect(res.json).toHaveBeenCalledWith({ error: 'Internal server error' });
    });
  });
});
