const { Slot } = require('../models');

async function hasCapacity(slotId, requestedGuests) {
  try {
    const slot = await Slot.findByPk(slotId);

    if (!slot) {
      throw new Error('Invalid slot_id');
    }

    return slot.capacity >= requestedGuests;
  } catch (error) {
    console.error(error);
    throw new Error('Error checking slot capacity');
  }
}

module.exports = {
  hasCapacity
};