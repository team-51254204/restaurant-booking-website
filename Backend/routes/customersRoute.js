const express = require('express');
const validateToken = require('../Middleware/validateTokenHandler'); 

const {registerUser, loginUser } = require('../controllers/customersController');
const router = express.Router();

router.post('/v1/auth/signup', registerUser);
router.post('/v1/auth/signin', loginUser);

module.exports = router;
