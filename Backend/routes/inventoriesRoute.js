const express = require('express');
const { createInventories, getInventoryByDetails } = require('../controllers/inventoriesControllers');

const router = express.Router();

// Define a route to create inventories
router.post('/inventories/:restaurantId', async (req, res) => {
  const { restaurantId } = req.params;

  try {
    // Convert restaurantId to an integer if needed
    const parsedRestaurantId = parseInt(restaurantId);

    await createInventories(parsedRestaurantId);
    return res.status(200).json({ message: 'Inventories created successfully.' });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: 'Internal server error' });
  }
});
router.get('/inventories/details', getInventoryByDetails);

module.exports = router;
