const express = require('express');
const slotsController = require('../controllers/slotsController'); // Update the import

const router = express.Router();

// Define a route to create slots
router.post('/slots/:restaurantId', async (req, res) => {
  const { restaurantId } = req.params;

  try {
    await slotsController.slots(parseInt(restaurantId)); // Update the usage
    return res.status(200).json({ message: 'Slots created successfully.' });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ error: 'Internal server error' });
  }
});

router.get('/getSlotByTime/:restaurantId/:time', slotsController.getSlotByTime);

module.exports = router;
