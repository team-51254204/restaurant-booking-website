const express = require('express');
const restaurantsController = require('../controllers/restaurantsController');
const validateToken = require('../Middleware/validateTokenHandler');

const router = express.Router();

router.get('/select', restaurantsController.selectAll);
router.get('/getDetailById/:restaurantId', restaurantsController.getDetailById);
router.post('/insert', restaurantsController.insert);
router.delete('/delete/:restaurantId', restaurantsController.delete);

module.exports = router;
