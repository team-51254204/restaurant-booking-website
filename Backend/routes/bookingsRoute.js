const express = require('express');
const reservationController = require('../controllers/bookingsController');
// const inventoriesController = require('../controllers/inventoriesController');

const validateToken = require('../Middleware/validateTokenHandler');

const reservationRouter = express.Router();

reservationRouter.post('/createBooking', validateToken, reservationController.createReservation);

module.exports = reservationRouter;