const asyncHandler = require("express-async-handler");
const jwt = require("jsonwebtoken");
const { COOKIE_OPTIONS } = require("../constants/apiConstant");

const validateToken = (req, res, next) => {
  let token;
  let authHeader = req.cookies?.AccessToken || req.headers.authorization;

  // Check if Authorization header is present
  if (!authHeader) {
    return res.status(401).json({ message: 'User is not authorized or token is missing' });
  }

  try {
    // Taking from cookies or headers
    token = authHeader;

    // Verify the token using the provided SECRET_KEY
    let verifiedUser;
    try {
      verifiedUser = jwt.verify(token, process.env.SECRET_KEY);
    } catch (error) {
      console.error('Token verification failed:', error);
      return res.status(401).send('Unauthorized request');
    }

    // Check if the token is valid
    if (!verifiedUser) {
      return res.status(401).send('Unauthorized request');
    }

    // Attach the user information from the token payload to the request object
    req.userId = verifiedUser.userId;

    next(); // Continue with the next middleware or route handler
  } catch (error) {
    console.error('Error during token validation:', error);
    res.status(500).json({ message: 'Internal server error' });
  }
};

module.exports = validateToken;
