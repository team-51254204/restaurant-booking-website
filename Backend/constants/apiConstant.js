const COOKIE_OPTIONS = {
  httpOnly: false,
  secure: true,
  sameSite: "none",
  methods: ["GET", "PUT", "POST", "DELETE", "OPTIONS"],
};

const CORS_OPTIONS = {
  origin: [
    "https://localhost:3000",
    "https://127.0.0.1:3000",
  ],
  credentials: true,
};

module.exports = { COOKIE_OPTIONS, CORS_OPTIONS };