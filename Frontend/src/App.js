import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import './App.css';

import HomePages from './components/pageContent/HomePages';
import Login from './components/pageContent/Login';
import Signup from './components/pageContent/SignUp';
import Restaurant from './components/pageContent/RestaurantDetails';
import Booking from './components/pageContent/Booking';
import Allrestaurants from './components/pageContent/AllRestaurants';
import NotFound from './components/pageContent/NotFound';
import {store} from './state/store'

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route path='/' exact component={HomePages} />
          <Route path="/Allrestaurants" exact component={Allrestaurants} />
          <Route path='/login' exact component={Login} />
          <Route path='/signup' exact component={Signup} />
          <Route path="/restaurant/:restaurantId" exact component={Restaurant} />
          <Route path="/booking" exact component={Booking} />
          <Route component={NotFound} />
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
