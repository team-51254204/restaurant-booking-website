import React from 'react'

const Footer = () => {
  return (
    <footer className="text-center text-white">
    <div className="container p-4 pb-0">
      <section>
        <p className="d-flex justify-content-center align-items-center">
          <span className="me-3">Copyright &copy;Team5</span><br></br>
        </p>
      </section>
    </div>
  </footer>
);
};

export default Footer;