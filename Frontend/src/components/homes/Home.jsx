import React from 'react';
import banner from '../../images/banner.png';
import Header from '../home/header/Header';
import { useHistory } from 'react-router-dom';
import Cookies from 'js-cookie';

const Home = () => {
  const history = useHistory();

  const handleScrollOrRedirect = () => {
    const isLoggedIn = Cookies.get("AccessToken")
  
    if (isLoggedIn) {
      const targetElement = document.getElementById('scroll-target');
      if (targetElement) {
        const currentPosition = window.scrollY;
  
        // Adjust this distance to scroll down
        const additionalScroll = 320;
  
        // Calculate the new scroll position
        const newScrollPosition = currentPosition + targetElement.getBoundingClientRect().top + additionalScroll;
  
        window.scrollTo({
          top: newScrollPosition,
          behavior: 'smooth',
        });
      }
    } else {
      history.push('/login');
    }
  };
  

  return (
    <>
      <section className="home" style={{ margin: 0, padding: 0 }}>
        <Header />
        <div className="container flex">
          <div className="left">
            <img src={banner} alt="Rest" />
          </div>
          <div className="right topMargin" id="scroll-target">
            <h1>Enjoy Best deals at our Booking Hub!!</h1>
            <h2>Save 50%</h2>
            <p>"Exclusive deals await! Unlock a world of savings as you book through our hub. Enjoy the best dining experience with incredible discounts.</p>
            <button type="button" className="btn btn-warning" onClick={handleScrollOrRedirect}>
              {Cookies.get("AccessToken") ? 'Book Today!' : 'Find the Best Deals'}
            </button>
          </div>
        </div>
      </section>
    </>
  );
};

export default Home;
