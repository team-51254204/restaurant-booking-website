import React from 'react';
import { withRouter } from 'react-router-dom';

const Cards = ({ history, id, title, description, imageUrl, cuisineType }) => {
  const handleClick = () => {
    if (id) {
      history.push(`/restaurant/${id}`);
    } else {
      console.error("Cannot navigate to undefined restaurant id");
    }
  };

  return (
    <div className="my-3">
      <div className="card">
        <img className="card-img-top" src={imageUrl} alt="...." />
        <div className="card-body">
          <h5 className="card-title">{title}</h5>
          <p className="card-text">
            {description} | Cuisine: {cuisineType}
          </p>
          <button className="btn btn-danger" onClick={handleClick}>
            See Details
          </button>
        </div>
      </div>
    </div>
  );
};

export default withRouter(Cards);
