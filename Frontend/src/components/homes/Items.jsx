import React, { useState, useEffect } from 'react';
import { Link, useLocation } from 'react-router-dom';
import Cards from './Cards';

const Items = () => {
  const location = useLocation();
  const [list, setList] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [pageSize, setPageSize] = useState(4);
  const [error, setError] = useState(null);
  const [noResults, setNoResults] = useState(false);
  const searchQuery = new URLSearchParams(location.search).get('queryParam');

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);

      try {
        let apiUrl = 'http://localhost:3001/select';

        if (searchQuery) {
          const encodedQuery = encodeURIComponent(searchQuery);
          apiUrl += `?queryParam=${encodedQuery}`;
          setCurrentPage(1);
        }

        const response = await fetch(apiUrl, {
          credentials:'same-origin',
          method: 'GET',
          headers: {
            'Content-type': 'application/json',
          },

        });

        if (!response.ok) {
          if (response.status === 404) {
            setNoResults(true);
          } else {
            throw new Error('Network response was not ok');
          }
        } else {
          const data = await response.json();
          setList(data.restaurants);
          setTotalPages(Math.ceil(data.restaurants.length / pageSize));
        }

        setLoading(false);
      } catch (error) {
        console.error('Error fetching data:', error);
        setError('An error occurred while fetching data.');
        setLoading(false);
      }
    };

    fetchData();

    return () => {
      setList([]);
      setError(null);
      setLoading(false);
      setNoResults(false);
    };
  }, [currentPage, searchQuery]);

  const handleNextPage = () => {
    if (currentPage < totalPages) {
      setCurrentPage((prevPage) => prevPage + 1);
    }
  };

  const handlePrevPage = () => {
    if (currentPage > 1) {
      setCurrentPage((prevPage) => prevPage - 1);
    }
  };

  const slicedList = list.slice((currentPage - 1) * pageSize, currentPage * pageSize);

  return (
    <div>
      {loading && <p>Loading...</p>}
      {noResults && <p className="Error-text">No restaurants found.</p>} {/* Display message for no results */}
      {error && <p>{error}</p>}

      <div className="container my-3">
        <div className="row">
          {slicedList.map((element) => (
            <div className="col-md-6" key={element.id}>
              <Link to={`/restaurant/${element.id}`}>
                <Cards
                  id={element.id}
                  title={element.name}
                  description={element.location}
                  imageUrl={element.image}
                  cuisineType={element.cuisine_type}
                />
              </Link>
            </div>
          ))}
        </div>

        <div className="row">
          <div className="col-md-12 text-right">
            <nav aria-label="Page navigation">
              <ul className="pagination">
                <li className={`page-item ${currentPage === 1 ? 'disabled' : ''}`}>
                  <button className="page-link" onClick={handlePrevPage}>
                    « Previous
                  </button>
                </li>

                <li className={`page-item ${currentPage === totalPages ? 'disabled' : ''}`}>
                  <button className="page-link" onClick={handleNextPage}>
                    Next »
                  </button>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Items;
