import React, { useState } from "react";
import { Link, useHistory, useLocation } from "react-router-dom";
import Cookies from 'js-cookie';

const Header = () => {
  const history = useHistory();
  const location = useLocation();
  const [searchQuery, setSearchQuery] = useState("");

  const handleLogout = () => {
    Cookies.remove("AccessToken")
    history.push("/");
  };

  const handleLoginSignupClick = () => {
    history.push("/login");
  };

  const handleSearchSubmit = (e) => {
    e.preventDefault();

    if (searchQuery.trim() === "") {
      history.push("/AllRestaurants");
    } else {
      const encodedSearchQuery = encodeURIComponent(searchQuery.trim());
      history.push(`/AllRestaurants?queryParam=${encodedSearchQuery}`);
    }
  };

  const isLoggedIn = Cookies.get("AccessToken");

  const isActive = (path) => {
    return location.pathname === path ? "active" : "";
  };

  return (
    <>
      <div className="navbar navbar-inverse">
        <div className="container-fluid">
          <div className="row">
            <div className="col-lg-12">
              <div className="navbar-header">
                <button
                  className="navbar-toggle"
                  data-target="#mobile_menu"
                  data-toggle="collapse"
                >
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                </button>
                <Link to="/" className="navbar-brand">
                  Taste-Trek
                </Link>
              </div>

              <div className="navbar-collapse collapse" id="mobile_menu">
                <ul className="nav navbar-nav">
                  <li className={isActive("/")}>
                    <Link to="/">Home</Link>
                  </li>
                  <li className={isActive("/AllRestaurants")}>
                    <Link to="/AllRestaurants">Browse Restaurants</Link>
                  </li>
                </ul>

                <ul className="nav navbar-nav">
                  <li>
                    <form onSubmit={handleSearchSubmit} className="navbar-form">
                      <div className="form-group">
                        <div className="input-group">
                          <input
                            type="search"
                            name="search"
                            placeholder="Search Anything Here..."
                            className="form-control"
                            style={{ width: "500px" }}
                            value={searchQuery}
                            onChange={(e) => setSearchQuery(e.target.value)}
                          />

                          <span
                            className="input-group-addon btn btn-default btn-search"
                            style={{ cursor: "pointer" }}
                            onClick={handleSearchSubmit}
                          >
                            <span className="glyphicon glyphicon-search"></span>{" "}
                            Search
                          </span>
                        </div>
                      </div>
                    </form>
                  </li>
                </ul>

                <ul className="nav navbar-nav navbar-right">
                  {isLoggedIn ? (
                    <>
                      <li>
                        <a href="" onClick={handleLogout}>
                          <span className="glyphicon glyphicon-log-in"></span>{" "}
                          Logout
                        </a>
                      </li>
                    </>
                  ) : (
                    <li>
                      <a href="" onClick={handleLoginSignupClick}>
                        <span className="glyphicon glyphicon-log-in"></span>{" "}
                        Login/SignUp
                      </a>
                    </li>
                  )}
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Header;
