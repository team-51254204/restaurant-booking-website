import React from "react";
import { useHistory } from "react-router-dom";
import Footer from "../homes/Footer";

const ReservationDetails = () => {
  const history = useHistory();
  const reservationData = history.location.state?.reservation;

  const handleBack = () => {
    history.goBack();
  };

  const formatBookingDate = (dateString) => {
    const options = { year: "numeric", month: "2-digit", day: "2-digit" };
    const date = new Date(dateString.replace(/-/g, "/"));
    const formattedDate = date.toLocaleDateString("en-GB", options);
    
    date.setHours(date.getHours() - 5);
    date.setMinutes(date.getMinutes() - 30);
  
    const optionsTime = { hour: "2-digit", minute: "2-digit", hour12: true };
    const formattedTime = date.toLocaleTimeString("en-US", optionsTime);
  
    return { formattedDate, formattedTime };
  };
  
  const formattedDateTime = reservationData
    ? formatBookingDate(reservationData.booking_date)
    : window.location.href = '/404-not-found';

  return (
    <>
      <div className="reservation-details">
        {reservationData ? (
          <>
            <div className="content1">
              <i className="fa fa-check-circle" aria-hidden="true"></i>
              <h1>Yay! Booking Confirmed</h1>
              <h1>Booking Details:</h1>
            </div>
            <p><strong>Restaurant:</strong> {reservationData.restaurant_name}</p>
            <p><strong>Location:</strong> {reservationData.restaurant_location}</p>
            <p><strong>Cuisine Type:</strong> {reservationData.cuisine_type}</p>
            <p><strong>Customer Name:</strong> {reservationData.customer_name}</p>
            <p><strong>Number of Guests:</strong> {reservationData.num_guests}</p>
            <p><strong>Booking Date:</strong> {formattedDateTime.formattedDate}</p>
            <p><strong>Booking Time:</strong> {formattedDateTime.formattedTime}</p>
            <br></br>
            <br></br>
            <p><strong>Note:</strong> Please reach the restaurant 10 mins earlier for a better experience.</p>
          </>
        ) : (
          <p>No reservation data available</p>
        )}
        <div className="button-container">
          {reservationData ? (
            <button className="btn btn-primary" onClick={handleBack}>
              Back to Restaurant
            </button>
          ) : null}
        </div>
      </div>
      <Footer />
    </>
  );
};

export default ReservationDetails;
