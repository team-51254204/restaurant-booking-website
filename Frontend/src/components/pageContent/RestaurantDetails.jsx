import React, { useEffect, useState } from "react";
import Header from "../home/header/Header";
import { Link, useHistory } from "react-router-dom";
import Deal from "../../images/Deal.jpeg";
import Swal from "sweetalert2";
import Footer from "../homes/Footer";
import { jwtDecode } from "jwt-decode";
import Cookies from 'js-cookie';

const RestaurantDetails = ({ match }) => {
  const [restaurant, setRestaurant] = useState({});
  const [date, setDate] = useState("");
  const [timeSlot, setTimeSlot] = useState();
  const [guest, setGuest] = useState("2");
  const [contactNumber, setContactNumber] = useState("");
  const [loading, setLoading] = useState(true);
  const [today, setToday] = useState("");
  const [tillTheDate, setTillTheDate] = useState("")
  const [slotId, setSlotId] = useState("");
  const [reservationData, setReservationData] = useState(null);
  const [bookingError, setBookingError] = useState(null);
  const [inventoryQuantity, setQuantity] = useState();
  const restaurantId = match.params.restaurantId;
  const history = useHistory();

  const token = Cookies.get("AccessToken");
  useEffect(() => {
    window.scrollTo(0, 0);
    const fetchInventoryDetails = async (restaurantId, slotId, date) => {
      try {
        const inventoryResponse = await fetch(
          `http://localhost:3001/inventories/details?restaurant_id=${restaurantId}&slot_id=${slotId}&date=${date}`
        );
        if (!inventoryResponse.ok) {
          throw new Error(`Network response was not ok: ${inventoryResponse.status}`);
        }

        const inventoryData = await inventoryResponse.json();
        console.log(inventoryData);
        setQuantity(inventoryData.quantity);
      } catch (error) {
        console.error("Error fetching inventory details:", error.message);
        setQuantity(0);
      }
    };

    const fetchRestaurantDetails = async () => {
      try {
        const restaurantResponse = await fetch(
          `http://localhost:3001/getDetailById/${restaurantId}`
        );
        if (!restaurantResponse.ok) {
          if (
            restaurantResponse.status === 500 ||
            restaurantResponse.status === 404
          ) {
            console.error(
              "Internal Server Error. Redirecting to Not Found page."
            );
            window.location.href = "/404-not-found";
            return;
          }
          throw new Error(
            `Network response was not ok: ${restaurantResponse.status}`
          );
        }

        const restaurantData = await restaurantResponse.json();
        console.log("Restaurant Data:", restaurantData);

        const timeSlotResponse = await fetch(
          `http://127.0.0.1:3001/getSlotByTime/${restaurantId}/${timeSlot || generateTimeSlots()[0]
          }`
        );
        if (!timeSlotResponse.ok) {
          throw new Error(
            `Network response was not ok: ${timeSlotResponse.status}`
          );
        }

        const timeSlotData = await timeSlotResponse.json();
        console.log("Time Slot:", timeSlotData.start_time);
        console.log("Slot Data:", timeSlotData);
        console.log("Selected Date:", date)

        setSlotId(timeSlotData.id.toString());
        setRestaurant(restaurantData);

        if (restaurantId && timeSlot && date) {
          await fetchInventoryDetails(restaurantId, timeSlotData.id.toString(), date);
        }

      } catch (error) {
        console.error("Error fetching restaurant details:", error.message);
      } finally {
        setLoading(false);
      }
    };

    const todayDate = new Date().toISOString().split("T")[0];
    setToday(todayDate);

    fetchRestaurantDetails();
  }, [restaurantId, timeSlot, date]);

  const Confirm = async (formattedBookingDate) => {
    try {
      const decodedToken = jwtDecode(token);
      const customer_id = decodedToken.customer_id;
      const customer_name = decodedToken.customer_name;

      console.log(decodedToken)

      const requestBody = {
        start_time: timeSlot,
        slot_id: slotId,
        restaurant_id: restaurantId,
        customer_id: customer_id.toString(),
        customer_name: customer_name,
        contact_number: contactNumber,
        booking_date: formattedBookingDate,
        num_guests: guest,
      };

      console.log("start_time:", timeSlot);
      console.log("slot_id:", slotId);
      console.log("customer_id:", customer_id.toString());
      console.log("customer_name:", customer_name);
      console.log("restaurant_name:", restaurant.name);
      console.log("contact_number:", contactNumber);
      console.log("booking_date:", formattedBookingDate);
      console.log("num_guests:", guest);

      const result = await fetch("http://127.0.0.1:3001/createBooking", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: token,
        },
        body: JSON.stringify(requestBody),
        credentials: "include",
      });

      if (!result.ok) {
        throw new Error(`Network response was not ok: ${result.status}`);
      }

      const data = await result.json();

      setReservationData({ ...data.reservation, customer_id, customer_name });
      setBookingError(null);
      history.push("/booking", { reservation: { ...data.reservation, customer_id, customer_name } });
    } catch (error) {
      console.error("Error creating booking:", error.message);
      setBookingError("Error during Reservation!!");
    }
  };

  const formatTime = (date) => {
    let hours = date.getHours();
    const minutes = date.getMinutes();
    const period = hours >= 12 ? "PM" : "AM";

    const formattedHours = hours < 10 ? `0${hours}` : hours.toString();
    const formattedMinutes = minutes === 0 ? "00" : minutes.toString();

    return `${formattedHours}:${formattedMinutes} ${period}`;
  };

  const generateTimeSlots = () => {
    const currentTime = new Date();
    const currentIST = new Date(
      currentTime.toLocaleString("en-US", { timeZone: "Asia/Kolkata" })
    );

    currentIST.setMinutes(0, 0, 0, 0);

    const openingTime = new Date(currentIST);
    openingTime.setHours(9, 0, 0, 0);

    const closingTime = new Date(currentIST);
    closingTime.setHours(23, 59, 0, 0);

    const timeSlots = [];
    const numberOfSlots = 5;

    if (currentIST.getHours() === 12) {
      currentIST.setHours(12, 0, 0, 0);
    }

    if (currentIST.getMinutes() >= 30) {
      currentIST.setHours(currentIST.getHours() + 1, 0, 0, 0);
    } else {
      currentIST.setMinutes(30);
    }

    if (currentIST >= openingTime) {
      currentIST.setMinutes(currentIST.getMinutes() + 30);
    }

    if (currentIST.toISOString().split("T")[0] < date) {
      while (openingTime <= closingTime) {
        const option = formatTime(openingTime);
        timeSlots.push(option);
        openingTime.setMinutes(openingTime.getMinutes() + 30);
      }
    } else {

      let count = 0;
      while (count < numberOfSlots) {
        const option = formatTime(currentIST);
        timeSlots.push(option);
        currentIST.setMinutes(currentIST.getMinutes() + 30);
        count++;
      }
    }

    return timeSlots;
  };

  const handleGuestChange = (e) => {
    const value = e.target.value;
    if (value >= 1 && value <= 5) {
      setGuest(value);
    } else {
      console.error("Number of guests must be between 1 and 5");
    }
  };

  const formatBookingDate = (selectedDate, selectedTime) => {
    try {
      const dateParts = selectedDate.split("-");
      const timeParts = selectedTime.split(":");

      const formattedDate = new Date(
        Date.UTC(
          parseInt(dateParts[0]),
          parseInt(dateParts[1]) - 1,
          parseInt(dateParts[2]),
          parseInt(timeParts[0]),
          parseInt(timeParts[1])
        )
      );

      formattedDate.setHours(formattedDate.getHours() + 5);
      formattedDate.setMinutes(formattedDate.getMinutes() + 30);

      const formattedDateString = formattedDate
        .toISOString()
        .slice(0, 19)
        .replace("T", " ");

      console.log("Formatted Booking Date:", formattedDateString);
      return formattedDateString;
    } catch (error) {
      console.error("Error formatting booking date:", error.message);
      return null;
    }
  };

  const handleConfirm = () => {
    if (!token) {
      setBookingError(
        <span>
          Please <Link to="/Login" style={{ color: 'red' }}>Login!</Link>
        </span>
      );
      const loginElement = document.getElementById("Login");
      if (loginElement) {
        loginElement.classList.add("error-input");
      }
      return;
    }

    if (!contactNumber || contactNumber.length !== 10) {
      setBookingError("Please enter a valid 10-digit contact number");
      const contactNumberElement = document.getElementById("contactNumber");
      if (contactNumberElement) {
        contactNumberElement.classList.add("error-input");
      }
      return;
    }

    const decodedToken = jwtDecode(token);
    const customer_id = decodedToken.customer_id;
    const customer_name = decodedToken.customer_name;

    console.log(decodedToken)

    Swal.fire({
      title: "Confirm Booking",
      html: `
        <hr>
        <table style="margin: 0 auto; text-align: left;">
          <tr>
            <td><strong>Restaurant:</strong></td>
            <td style="text-align: right;">${restaurant.name}</td>
          </tr>
          <tr>
            <td><strong>Location:</strong></td>
            <td style="text-align: right;">${restaurant.location}</td>
          </tr>
          <tr>
            <td><strong>Cuisine Type:</strong></td>
            <td style="text-align: right;">${restaurant.cuisine_type}</td>
          </tr>
          <tr>
            <td><strong>Customer Name:</strong></td>
            <td style="text-align: right;">${customer_name}</td>
          </tr>
          <tr>
            <td><strong>Contact Number:</strong></td>
            <td style="text-align: right;">${contactNumber}</td>
          </tr>
          <tr>
            <td><strong>Number of Guests:</strong></td>
            <td style="text-align: right;">${guest}</td>
          </tr>
          <tr>
            <td><strong>Booking Date:</strong></td>
            <td style="text-align: right;">${date}</td>
          </tr>
          <tr>
            <td><strong>Booking Time:</strong></td>
            <td style="text-align: right;">${timeSlot}</td>
          </tr>
        </table>
        <hr>
      `,
      icon: "question",
      showCancelButton: true,
      confirmButtonText: "Confirm Booking",
      cancelButtonText: "Edit Details",
    }).then((result) => {
      if (result.isConfirmed) {
        const formattedBookingDate = formatBookingDate(date, timeSlot);
        Confirm(formattedBookingDate);
      }
    });
  };

  return (
    <>
      <Header />
      <div className="row">
        <div className="col-sm-8">
          <div className="restro-wrap">
            <img src={restaurant.image} alt="Rest" />
            <div className="content">
              <h3>
                {restaurant.name}
                <span className="star-icon">
                  <i className="fas fa-star"></i>
                  <i className="fas fa-star"></i>
                  <i className="fas fa-star"></i>
                  <i className="fas fa-star"></i>
                  <i className="fas fa-star"></i>
                </span>
              </h3>
              <i className="fas fa-map-marker-alt"></i> {restaurant.location}
              <br />
              <i className="fas fa-wine-glass"></i> Cuisine:{" "}
              {restaurant.cuisine_type}
              <br />
              <hr />
              <h4>Description</h4>
              <hr />
              <p>
                Step into a world of culinary excellence at our inviting eatery.
                Our menu, crafted with the finest local and international
                ingredients, promises a journey of flavors. Whether you crave
                classic favorites or adventurous creations, our diverse
                offerings cater to every palate. The ambiance blends
                sophistication and comfort, providing an ideal setting for
                intimate dinners or casual gatherings. Immerse yourself in an
                atmosphere where every detail is designed to enhance your dining
                experience. Join us for a gastronomic adventure and savor
                delightful dishes prepared by our passionate chefs. Your
                memorable culinary journey awaits at our cherished
                establishment. Welcome to a haven of exceptional food and
                inviting ambiance.
              </p>
            </div>
            <hr />
            <div className="gallery">
              <div className="row">
                <div className="col-md-3">
                  <div className="gallery-img">
                    <img
                      src={
                        "https://d1csarkz8obe9u.cloudfront.net/posterpreviews/valentine-coffee%2Ccoffee-menu%2Cevent-design-template-7355cb647f831219b78b8087a73dab15_screen.jpg?ts=1698513369"
                      }
                      alt="try"
                    />
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="gallery-img">
                    <img
                      src={
                        "https://d1csarkz8obe9u.cloudfront.net/posterpreviews/restaurant-offers-design-template-9755c79fd6cd3510238e2c246f09b430_screen.jpg?ts=1599490952"
                      }
                      alt="try"
                    />
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="gallery-img">
                    <img
                      src={
                        "https://d1csarkz8obe9u.cloudfront.net/posterpreviews/coconut-drinks-template-design-353dba2dc50fb3adbc9edcb1bfdcb8de.jpg?ts=1685604518"
                      }
                      alt="try"
                    />
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="gallery-img">
                    <img
                      src={
                        "https://d1csarkz8obe9u.cloudfront.net/posterpreviews/arabic-cuisine-restaurant-menu-ad-design-template-2183f74d426133cefa21b31e4cad9dbd_screen.jpg?ts=1678468484"
                      }
                      alt="try"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-sm-4">
          <div className="form-wrap">
            <h3>Book Your Table</h3>
            <div className="container-form">
              <div className="form-row">
                <div className="col-12">
                  <label htmlFor="date">Select Date:</label>
                  <input
                    id="date"
                    type="date"
                    className="form-control"
                    value={date}
                    min={today}
                    // max={tillTheDate}
                    onChange={(e) => setDate(e.target.value)}
                  />
                </div>
              </div>
              <div className="form-row">
                <label htmlFor="timeSlot">Select Time Slot:</label>
                <select
                  id="timeSlot"
                  className="form-control"
                  value={timeSlot}
                  onChange={(e) => setTimeSlot(e.target.value)}
                >
                  <option value="">Select Time Slot</option>
                  {generateTimeSlots().map((slot, index) => (
                    <option key={index} value={slot}>
                      {slot}
                    </option>
                  ))}
                </select>
              </div>
              <div className="form-row">
                <label htmlFor="guest">Number of Guests:</label>
                <input
                  id="guest"
                  type="number"
                  className="form-control"
                  min="1"
                  max="5"
                  value={guest}
                  onChange={handleGuestChange}
                />
              </div>
              <div className="form-row">
                <label htmlFor="contactNumber">Contact Number:</label>
                <input
                  id="contactNumber"
                  type="tel"
                  placeholder="XXXXXXXXXX"
                  className="form-control"
                  pattern="[0-9]{10}"
                  value={contactNumber}
                  onChange={(e) => setContactNumber(e.target.value)}
                />
              </div>
              <div className="form-row">
                {bookingError && (
                  <div className="error-message">{bookingError}</div>
                )}
                <button
                  className="btn book_table"
                  onClick={handleConfirm}
                  disabled={!guest || guest < 1 || guest > 5 || !timeSlot || !date}
                >
                  Book Table
                </button>
                {date && timeSlot && (
                  <>
                    <p className="hurry">Tables Left: {inventoryQuantity} only</p>
                    <div className="form-img">
                      <img src={Deal} alt="Rest" />
                    </div>
                  </>
                )}
                <div className="form-img">
                  <img src={Deal} alt="Rest" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default RestaurantDetails;