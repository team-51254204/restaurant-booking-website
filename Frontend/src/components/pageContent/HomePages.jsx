import React from 'react'
import Home from "../homes/Home"
import Items from '../homes/Items'
import Footer from '../homes/Footer'
import { Pick } from '../homes/Pick'

const HomePages = () => {
  return (
    <>
      <Home />
      <Pick />
      <Items />
      <Footer />
    </>
  )
}
export default HomePages;
