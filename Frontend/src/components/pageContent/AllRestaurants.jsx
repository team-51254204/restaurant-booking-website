import React from 'react'
import Items from '../homes/Items'
import Header from '../home/header/Header'
const AllRestaurants = () => {
  return (
    <>
    <Header />
    <Items />
    </>
  )
}
export default AllRestaurants;
