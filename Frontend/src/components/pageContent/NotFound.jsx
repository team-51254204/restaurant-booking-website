import React from 'react';

const NotFound= () => {
  return (
    <div className='not-found'>
      <div className="page-404">
        <div className="outer">
          <div className="middle">
            <div className="inner">
              <div className="inner-circle">
                <i className="fa fa-home"></i>
                <span>404</span>
              </div>
              <span className="inner-status">Oops! You're lost</span>
              <span className="inner-detail">
                We cannot find the page you're looking for.
                <a href="/" className="btn book_table">
                  <i className="fa fa-home"></i>&nbsp; Return home
                </a>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default NotFound;
