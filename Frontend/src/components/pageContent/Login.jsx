import React, { useEffect, useCallback, useState } from "react";
import Header from "../home/header/Header";
import { Link, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import Cookies from 'js-cookie';
import { jwtDecode } from "jwt-decode";

import {
  setEmail,
  setPassword,
  setError,
  setEmailValid,
  setEmailErrorMessage,
  clearCredentials,
} from "../../state/action-creators/authActions";

 /* global google */
const Login = () => {
  const [user, setUser] = useState({});

  const dispatch = useDispatch();
  const { email, password, error, emailValid, emailErrorMessage } = useSelector((state) => state.auth);
  const history = useHistory();

  useEffect(() => {
    const token = Cookies.get("AccessToken");
    if (token) {
      history.push("/");
    }

    const handleCallbackResponse = (res) => {
      console.log("Encoded JWT Token:", res.credential);
      const user_object = jwtDecode(res.credential);
      setUser(user_object)
      console.log(user_object.email);
      console.log(user_object.sub)
      
      document.getElementById("signInDiv").hidden = true
    }
    google.accounts.id.initialize({
      client_id: "867380390572-thc3ap8lvpvmr7u7g82576cgvkof4f21.apps.googleusercontent.com",
      callback: handleCallbackResponse
    })

    google.accounts.id.renderButton(
      document.getElementById("signInDiv"),
      { theme: "outline", size: "large" }
    )
  }, [history]);

  const validateEmail = useCallback(() => {
    const emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    const isValid = emailRegex.test(email);
    dispatch(setEmailValid(isValid));
    dispatch(setEmailErrorMessage(isValid ? "" : "Please enter a valid email address"));
  }, [email, dispatch]);

  const login = useCallback(async () => {
    if (!email && !password) {
      dispatch(setError("Please enter your email and password"));
      return;
    }

    if (!password) {
      dispatch(setError("Please enter your password"));
      return;
    }

    validateEmail();

    if (!emailValid) {
      return;
    }

    try {
      const result = await fetch("http://localhost:3001/v1/auth/signin", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        credentials: 'include',
        body: JSON.stringify({ email, password }),
      });

      if (!result.ok) {
        const errorData = await result.json();
        dispatch(setError(errorData.message));
        return;
      }

      dispatch(clearCredentials());
      history.push("/");
    } catch (error) {
      console.error("An error occurred during login:", error);
      dispatch(setError("An error occurred during login. Please try again later."));
    }
  }, [email, password, emailValid, dispatch, history, validateEmail]);

  return (
    <div>
      <Header />
      <div className="wrapper">
        <h1>Login</h1>
        {error && <div className="alert alert-danger">{error}</div>}
        <input
          type="text"
          placeholder="Enter Your email"
          onChange={(e) => {
            dispatch(setEmail(e.target.value));
            validateEmail();
          }}
          onBlur={validateEmail}
          className={`form-control ${!emailValid ? "error" : ""}`}
        />
        <div className="error-message-email">{emailErrorMessage}</div>
        <br />
        <input
          type="password"
          placeholder="Enter password"
          onChange={(e) => {
            dispatch(setPassword(e.target.value));
          }}
          className="form-control"
        />
        <br />
        <button onClick={login} className="btn btn-primary">
          Login
        </button>
        <div className="member">
          Not a member?
          <Link to="/signup"> Register</Link>
        </div>

        <div id="signInDiv"></div>
        {user &&
          <div>
            <img src={user.picture}></img>
            <h3>{user.name}</h3>

          </div>}
      </div>
    </div>
  );
};

export default Login;
