import React, { useEffect, useCallback, useState } from "react";
import Header from "../home/header/Header";
import { Link, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import Cookies from 'js-cookie';
import { jwtDecode } from "jwt-decode";

import {
  setName,
  setEmail,
  setPassword,
  setError,
  setEmailValid,
  setEmailErrorMessage,
  clearCredentials,
} from "../../state/action-creators/authActions";

/* global google */
const Signup = () => {
  const [user, setUser] = useState({});

  const dispatch = useDispatch();
  const { name, email, password, error, emailValid, emailErrorMessage } = useSelector((state) => state.auth);
  const history = useHistory();

  useEffect(() => {
    const token = Cookies.get("AccessToken");
    if (token) {
      history.push("/");
    }

    const handleCallbackResponse = (res) => {
      console.log("Encoded JWT Token:", res.credential);
      const user_object = jwtDecode(res.credential);
      setUser(user_object);
      console.log(user_object);

      document.getElementById("signUpDiv").hidden = true;
    }
    google.accounts.id.initialize({
      client_id: "867380390572-thc3ap8lvpvmr7u7g82576cgvkof4f21.apps.googleusercontent.com",
      callback: handleCallbackResponse
    })

    google.accounts.id.renderButton(
      document.getElementById("signUpDiv"),
      { theme: "outline", size: "large" }
    )
  }, [history]);

  const validateEmail = useCallback(() => {
    const emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    const isValid = emailRegex.test(email);
    dispatch(setEmailValid(isValid));
    dispatch(setEmailErrorMessage(isValid ? "" : "Please enter a valid email address"));
  }, [email, dispatch]);

  const signup = useCallback(async () => {
    try {
      const result = await fetch("http://localhost:3001/v1/auth/signup", {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        credentials: 'include',
        body: JSON.stringify({ name, email, password }),
      });

      if (!result.ok) {
        const errorData = await result.json();
        dispatch(setError(errorData.message));
        return;
      }

      const data = await result.json();
      dispatch(clearCredentials());

      history.push("/");
    } catch (error) {
      console.error("Signup failed:", error);
      dispatch(setError("An error occurred during signup. Please try again later."));
    }
  }, [name, email, password, dispatch, history]);

  return (
    <div>
      <Header />
      <div className="wrapper">
        <h1>Signup</h1>
        {error && <div className="alert alert-danger">{error}</div>}
        <input
          type="text"
          placeholder="Enter Your name"
          onChange={(e) => dispatch(setName(e.target.value))}
          className="form-control"
        />
        <br />
        <input
          type="text"
          placeholder="Enter Your email"
          onChange={(e) => {
            dispatch(setEmail(e.target.value));
            validateEmail();
          }}
          onBlur={validateEmail}
          className={`form-control ${!emailValid ? "error" : ""}`}
        />
        <div className="error-message-email">{emailErrorMessage}</div>
        <br />
        <input
          type="password"
          placeholder="Enter password"
          onChange={(e) => {
            dispatch(setPassword(e.target.value));
          }}
          className="form-control"
        />
        <br />
        <button onClick={signup} className="btn btn-primary">
          Signup
        </button>
        <div className="member">
          Already a member? <Link to="/login">Login</Link>
        </div>

        <div id="signUpDiv"></div>
        {user && (
          <div>
            <img src={user.picture} alt="User profile"></img>
            <h3>{user.name}</h3>
          </div>
        )}
      </div>
    </div>
  );
};

export default Signup;
