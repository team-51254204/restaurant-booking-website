import * as actionTypes from '../actionTypes';

const initialState = {
  name: '',
  email: '',
  password: '',
  error: '',
  emailValid: true,
  emailErrorMessage: '',
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_NAME:
      return { ...state, name: action.payload };
    case actionTypes.SET_EMAIL:
      return { ...state, email: action.payload };
    case actionTypes.SET_PASSWORD:
      return { ...state, password: action.payload };
    case actionTypes.SET_ERROR:
      return { ...state, error: action.payload };
    case actionTypes.SET_EMAIL_VALID:
      return { ...state, emailValid: action.payload };
    case actionTypes.SET_EMAIL_ERROR_MESSAGE:
      return { ...state, emailErrorMessage: action.payload };
    case actionTypes.CLEAR_CREDENTIALS:
      return initialState;
    default:
      return state;
  }
};

export default authReducer;
